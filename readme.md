Run this script to install tools & configurations on a new server


## Usage
Install all
```
curl -fsSL https://gitlab.com/rajesh.neputer/new-server/-/raw/master/install.sh | bash
```

Install tmux only
```
curl -fsSL https://gitlab.com/rajesh.neputer/new-server/-/raw/master/install.sh | bash -s setup_tmux
```

Install omz and other misc
```
curl -fsSL https://gitlab.com/rajesh.neputer/new-server/-/raw/master/install.sh | bash -s setup_misc
```

----

### Create User
```
curl -fsSL https://gitlab.com/rajesh.neputer/new-server/-/raw/master/add_user.sh | user=ubuntu password=ubuntu bash
```

----

### Create Swap
```
curl -fsSL https://gitlab.com/rajesh.neputer/new-server/-/raw/master/create_swap.sh | SWAP_SIZE=2G sh -
```