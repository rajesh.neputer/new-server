#!/bin/bash

#### required vars
#CF_BEARER_TOKEN
#CF_ZONE_ID

# purge cloudflare cache
if [[ -n $CF_ZONE_ID ]] && [[ -n $CF_BEARER_TOKEN ]]; then
    echo "purging cloudflare cache...";
    curl -X POST "https://api.cloudflare.com/client/v4/zones/$CF_ZONE_ID/purge_cache" \
        -H "Authorization: Bearer $CF_BEARER_TOKEN" \
        -H "Content-Type: application/json" \
        --data '{"purge_everything":true}';
else
    echo "No CF zone ID or Bearer token found."
fi
