#!/usr/bin/env bash
set -e

# set variables
LINUX_USER=$(whoami)

# colors
GREEN='\033[0;32m'
NC='\033[0m' # No Color


# echo $LINUX_USER
# exit 1;

help() {
  cat << EOF
usage: $0 [OPTIONS]
    --help               Show this message
EOF
}

install_packages() {
  sudo apt update
  sudo apt-get install -y git tmux zsh xclip net-tools mycli ncdu neovim apache2-utils
}

setup_tmux() {
  # rm -rf ~/.tmux

  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
  
  echo "downloading tmux configuration..."
  curl -fsSL https://gitlab.com/rajesh.neputer/new-server/-/raw/master/config/tmux.conf -o ~/.tmux.conf
}

setup_misc() {
  # rm -rf ~/.oh-my-zsh ~/.fzf

  # install zsh if not installed
  which zsh
  if [[ $? -ne 0 ]]; then
    echo "installing zsh..."
    sudo apt-get install -y zsh
  fi

  # set omz as current shell
  curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh -o /tmp/omz-install.sh && echo "n" | bash /tmp/omz-install.sh

  # install fzf
  git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf && echo "y" | bash ~/.fzf/install
}

install_web() {
  sudo apt-get install -y nginx 

  # install php related packages
  sudo apt-get install -y composer php7.4 php7.4-{fpm,bcmath,curl,dev,gd,intl,mbstring,mysql,pgsql,xml,zip}
}

configure_web() {
  LINUX_USER=$(whoami)
  NGINX_MAIN_CONF=/etc/nginx/nginx.conf
  PHPCONF_DIR=/etc/php/7.4/fpm
  UPLOAD_MAX_FILESIZE=50M

  sudo sed -i -e "s/user www-data;/user $LINUX_USER;/" \
      -e "s/# server_tokens off;/server_tokens off; \n\t client_max_body_size $UPLOAD_MAX_FILESIZE;/" \
       $NGINX_MAIN_CONF

  # check nginx configuration
  sudo nginx -t

  sudo sed -i -e "s/post_max_size = 8M/post_max_size = $UPLOAD_MAX_FILESIZE/" \
    -e "s/upload_max_filesize = 2M/upload_max_filesize = $UPLOAD_MAX_FILESIZE/" \
    -e "s/memory_limit = 128M/memory_limit = -1/" \
    -e "s/max_execution_time = 30/max_execution_time = 180/" \
      $PHPCONF_DIR/php.ini

  sudo sed -i -e "s/user = www-data/user = $LINUX_USER/" \
    -e "s/group = www-data/group = $LINUX_USER/" \
    -e "s/listen.owner = www-data/listen.owner = $LINUX_USER/" \
    -e "s/listen.group = www-data/listen.group = $LINUX_USER/" \
      $PHPCONF_DIR/pool.d/www.conf

  # create directory for ssl
  sudo mkdir -p /etc/ssl/neputersite

  # create a demo nginx file
  
}

install_db() {
  sudo apt-get install -y mysql-server 
}

configure_db() {
  # create a database
  mysql -e "CREATE DATABASE main_db CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
  mysql -e "CREATE USER 'main_db_user'@'localhost' IDENTIFIED WITH mysql_native_password BY 'main_db_pass';";
  mysql -e "GRANT ALL PRIVILEGES ON main_db . * TO 'main_db_user'@'localhost';";
  mysql -e "FLUSH PRIVILEGES;"; 
}

generate_ssh() {
  if [[ ! -f ~/.ssh/id_ed25519 ]]; then
    echo "generating ssh key for the server...";
    ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/id_ed25519 -N ""

    echo "adding pub key for gitlab deploy..."
    mkdir -p ~/.ssh
    cat ~/.ssh/id_ed25519.pub >> ~/.ssh/authorized_keys
  else
    echo "ssh keys already exists"
  fi
}

# test() {
#   rm -rf ~/.fzf ~/.oh-my-zsh ~/.tmux
# }

if [[ -z "$@" ]]; then
  install_packages
  setup_tmux
  setup_misc
  install_web
  configure_web

  install_db
  configure_db

  generate_ssh
else
  $1
  exit 1
  $0
fi

for opt in "$@"; do
  case $opt in
    --help|-h)
      help
      exit 0
      ;;
    *)
      echo "unknown option: $opt"
      help
      exit 1
      ;;
  esac
done

echo -e "$GREEN All packages & configurations have been successfully set up $NC"
