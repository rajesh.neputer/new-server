#!/usr/bin/env bash
set -e

################################################################################
# Help                                                                         #
################################################################################
help() {
  cat << EOF
Create User
usage: $0 [OPTIONS]
  -h --help       Show this message
EOF
}

# user=""
# password=""

get_user_password() {
  user=$1
  password=$2
}


## Show help if input is -h or --help
for opt in "$@"; do
  case $opt in
    --help|-h)
      help
      exit 0
      ;;
    
    *)
  esac
done

# ask for user & password if value not exists
if [[ -z $SWAP_SIZE ]]; then
  SWAP_SIZE=2G
fi

echo "Creating Swap of size: $SWAP_SIZE "...

sudo swapon --show
free -h
df -h
sudo fallocate -l $SWAP_SIZE /swapfile
sudo chmod 600 /swapfile
ls -lh /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo swapon --show
free -h
sudo cp /etc/fstab /etc/fstab.bak
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab

echo "Performance tuning.."
cat /proc/sys/vm/swappiness
sudo sysctl vm.swappiness=10
cat /proc/sys/vm/vfs_cache_pressure
sudo sysctl vm.vfs_cache_pressure=50

cat << EOF | sudo tee -a /etc/sysctl.conf

# Performance tuning
vm.vfs_cache_pressure=50
vm.swappiness=10
EOF

echo "Swap Memory: $SWAP_SIZE created successfully"
