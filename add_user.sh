#!/usr/bin/env bash
set -e

################################################################################
# Help                                                                         #
################################################################################
help() {
  cat << EOF
Create User
usage: $0 [OPTIONS]
  -h --help       Show this message
  --user          mention user & password. ./add_user --user ram ram_password
EOF
}

# user=""
# password=""

get_user_password() {
  user=$1
  password=$2
}


## Show help if input is -h or --help
for opt in "$@"; do
  case $opt in
    --help|-h)
      help
      exit 0
      ;;
    # --user)
    #   get_user_password $2 $3
    #   ;;
    *)
    
      # echo "unknown option: $opt"
      # help
      # exit 1
      # ;;
  esac
done

# ask for user & password if value not exists
if [[ -z $user ]] || [[ -z $password ]]; then
  read -p 'Enter User: ' user
  read -s -p 'Enter Password: ' password
fi


  echo "Creating user..."
  sudo adduser --disabled-password --gecos "" ${user}
  echo "${user}:${password}" | sudo chpasswd

  echo "${user} ALL=(ALL:ALL) NOPASSWD: ALL" | sudo tee "/etc/sudoers.d/dont-prompt-${user}-for-sudo-password"

  # path is ~/.ssh/id_ed25519.pub
  sudo mkdir -p /home/${user}/.ssh \
  && echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMsf6KHLJKmHyYfRFw67wXDLqCt2sz8ycRbALkJsWsz+ rajesh@rajesh-ubuntu20" | sudo tee -a /home/${user}/.ssh/authorized_keys \
  && sudo chown -R ${user}:${user} /home/${user}/.ssh

  echo "New User: ${user} created successfully"
  


# echo -e "\n"

# sudo adduser --disabled-password --gecos "" ${user}
# echo "${user}:${password}" | sudo chpasswd

# echo "${user} ALL=(ALL:ALL) NOPASSWD: ALL" | sudo tee "/etc/sudoers.d/dont-prompt-${user}-for-sudo-password"

# # path is ~/.ssh/id_ed25519.pub
# sudo mkdir -p /home/${user}/.ssh \
# && echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMsf6KHLJKmHyYfRFw67wXDLqCt2sz8ycRbALkJsWsz+ rajesh@rajesh-ubuntu20" | sudo tee -a /home/${user}/.ssh/authorized_keys \
# && sudo chown -R ${user}:${user} /home/${user}/.ssh

# echo "New User: ${user} created successfully"